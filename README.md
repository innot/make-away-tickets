# Make Away Tickets #

A model plane ticket shop written in Java with Spring.

### Usage ##

To run a local version of the site, do the following.

1. Configure the database.

    You need MySQL or MariaDB.
    Create the database with the script at `db/create_msql.sql`.
    You can later drop that database with the `drop.sql` script in the same directory.
    Some sample data is provided in `fill_mysql.sql`. You can execute this script to
    put it into the created database.

2. Perform some setup.

    * `src/main/resources/hibernate.cfg.xml` contains settings
       Hibernate will use to connect to the database.
    * The `application.properties` file in the same directory
       contains parameters like BTC address where payments will be transferred,
       default administrator account and DB parameters for Spring
       (which should be same as in the prev. file).

3. Run the server:

    `mvn spring-boot:run`

    Maven will download the required packages and the site will
    be launced at localhost:8080.

### Screenshots ###

More screenshots are available in the `screenshots/` directory.

![Login and registration](https://bytebucket.org/innot/make-away-tickets/raw/d9532030ce2ec98c22a3ba0f06ac4801d73c110e/screenshots/10.png)

![Companies listing](https://bytebucket.org/innot/make-away-tickets/raw/d9532030ce2ec98c22a3ba0f06ac4801d73c110e/screenshots/11.png)

![Flight edit menu](https://bytebucket.org/innot/make-away-tickets/raw/d9532030ce2ec98c22a3ba0f06ac4801d73c110e/screenshots/13.png)

### Other ###

You can log into the control panel by typing administrator login and password
(root:root by default) into the user login form.

