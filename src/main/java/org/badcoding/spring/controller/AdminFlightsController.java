package org.badcoding.spring.controller;

import java.util.*;
import java.text.*;
import javax.servlet.http.*;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.annotation.*;
import org.badcoding.dao.interfaces.*;
import org.badcoding.hibernate.stored.*;
import org.badcoding.spring.form.*;

@Controller
@RequestMapping("/admin")
public class AdminFlightsController {
	@Autowired
	AirportsDAO airports;

	@Autowired
	TicketsDAO tickets;

	@Autowired
	FlightsDAO flights;

	@Autowired
	CompaniesDAO companies;

	@Autowired
	CitiesDAO cities;

	@Autowired
	PlanesDAO planes;

	private Boolean is_admin(HttpServletRequest request) {
		HttpSession session = request.getSession();
		return (session.getAttribute("is_admin") != null);
	}

	@RequestMapping("/flights")
	public String flights(HttpServletRequest request, Map<String, Object> model) {
		List<Integer> errors = new ArrayList<Integer>();
		if (!is_admin(request)) {
			errors.add(45);
			model.put("e", errors);
			return "redirect:/index";
		}
		model.put("flightsForm", new FlightsForm());
		return "/admin/flights";
	}

	@RequestMapping(value = "/flights_search", method = RequestMethod.GET)
	public String flights_search(@ModelAttribute FlightsForm flightsForm, HttpServletRequest request, Map<String, Object> model) {
		List<Integer> errors = new ArrayList<Integer>();
		List<Flights> result = new ArrayList<Flights>();
		if (!is_admin(request)) {
			errors.add(45);
			model.put("e", errors);
			return "redirect:/index";
		}
		try {
			String date0 = flightsForm.getStart();
			String date1 = flightsForm.getEnd();
			String city_out = flightsForm.getCity_out();
			String city_in = flightsForm.getCity_in();
			String company = flightsForm.getCompany();
			String id_s = flightsForm.getId();
			if (!id_s.equals("") && (!date0.equals("") || !date1.equals("") || !city_out.equals("") || !city_in.equals("") || !company.equals(""))) {
				errors.add(46);
				throw new Exception();
			}
			if (id_s != "") {
				Integer id = null;
				try {
					id = Integer.parseInt(id_s);
				} catch (Exception e) {
					errors.add(47);
					throw new Exception();
				}
				Flights t = flights.getById(id);
				if (t != null)
					result.add(t);
			} else {
				Date start = null;
				if (date0 != "")
					try {
						start = new SimpleDateFormat("dd.MM.yyyy", Locale.ENGLISH).parse(date0);
					} catch (Exception e) {
						try {
							start = new SimpleDateFormat("dd.MM.yyyy HH:mm", Locale.ENGLISH).parse(date0);
						} catch (Exception r) {
							errors.add(10);
						}
					}

				Date end = null;
				if (date1 != "")
					try {
						end = new SimpleDateFormat("dd.MM.yyyy", Locale.ENGLISH).parse(date1);
					} catch (Exception e) {
						try {
							end = new SimpleDateFormat("dd.MM.yyyy HH:mm", Locale.ENGLISH).parse(date1);
						} catch (Exception r) {
							errors.add(11);
						}
					}

				List<Cities> city0 = (city_out == "" ? new ArrayList<Cities>() : cities.getByTitle(city_out));
				if (city_out == "")
					city0.add(null);
				if (city0.size() == 0)
					errors.add(12);
				else if (city0.size() > 1)
					errors.add(13);

				List<Cities> city1 = (city_in == "" ? new ArrayList<Cities>() : cities.getByTitle(city_in));
				if (city_in == "")
					city1.add(null);
				if (city1.size() == 0)
					errors.add(14);
				else if (city1.size() > 1)
					errors.add(15);

				List<Companies> comp = (company == "" ? new ArrayList<Companies>() : companies.getByFilters(company, -1, -1, -1));
				if (company == "")
					comp.add(null);
				if (comp.size() == 0)
					errors.add(16);
				else if (comp.size() > 1)
					errors.add(17);

				if (errors.size() == 0) {
					result = flights.getByChoice(start, end, city0.get(0), city1.get(0), comp.get(0), false);
				} else {
					throw new Exception();
				}
			}

			if (result.size() == 0) {
				errors.add(48);
				model.put("info", errors);
			} else {
				model.put("results", result);
			}
		} catch (Exception e) {
			model.put("errors", errors);
		}
		model.put("flightsForm", flightsForm);
		return "/admin/flights";
	}

	@RequestMapping(value="/add_flight", method=RequestMethod.POST)
	public @ResponseBody List<Integer> add_flight(HttpServletRequest request, FlightsEditForm flightsEditForm) {
		List<Integer> errors = new ArrayList<Integer>();
		try {
			if (!is_admin(request)) {
				errors.add(45);
				throw new Exception();
			}
			String company_s = flightsEditForm.getCompany();
			String a0_s = flightsEditForm.getA0();
			String a1_s = flightsEditForm.getA1();
			String date_s = flightsEditForm.getDate();
			String length_s = flightsEditForm.getLength();
			String plane_s = flightsEditForm.getPlane();
			if (company_s == "" || a0_s == "" || a1_s == "" || plane_s == "" || date_s == "" || length_s == "") {
				errors.add(53);
				throw new Exception();
			}
			Integer company_id = null;
			Integer a0_id = null;
			Integer a1_id = null;
			Date date = null;
			Integer length = null;
			Integer plane_id = null;
			try {
				company_id = Integer.parseInt(company_s);
				a0_id = Integer.parseInt(a0_s);
				a1_id = Integer.parseInt(a1_s);
				length = Integer.parseInt(length_s);
				plane_id = Integer.parseInt(plane_s);
			} catch (Exception e) {
				errors.add(47);
				throw e;
			}
			try {
				date = new SimpleDateFormat("dd.MM.yyyy HH:mm").parse(date_s);
			} catch (Exception e) {
				errors.add(54);
			}
			Companies company = companies.getById(company_id);
			if (company == null) {
				errors.add(16);
			}
			Airports a0 = airports.getById(a0_id);
			if (a0 == null) {
				errors.add(55);
			}
			Airports a1 = airports.getById(a1_id);
			if (a1 == null) {
				errors.add(56);
			}
			Planes plane = planes.getById(plane_id);
			if (plane == null) {
				errors.add(57);
			}
			if (errors.size() != 0)
				throw new Exception();

			Flights c = new Flights();
			c.setCompany(company);
			c.setAirport_out(a0);
			c.setAirport_in(a1);
			c.setDate(date);
			c.setLength(length);
			c.setPlane(plane);
			flights.add(c);
		} catch (Exception e) {
			if (errors.size() == 0)
				errors.add(43);
		}
		return errors;
	}

	@RequestMapping(value="/get_flight", method=RequestMethod.GET)
	public @ResponseBody List<List<String>> get_flight(HttpServletRequest request, @RequestParam int flight_id) {
		List<String> errors = new ArrayList<String>();
		List<List<String>> result = new ArrayList<List<String>>();
		try {
			if (!is_admin(request)) {
				errors.add("45");
				throw new Exception();
			}
			Flights c = flights.getById(flight_id);
			if (c == null) {
				errors.add("48");
				throw new Exception();
			}
			List<String> t = new ArrayList<String>();
			t.add(Integer.toString(c.getFlight_id()));
			t.add(Integer.toString(c.getCompany().getCompany_id()));
			t.add(Integer.toString(c.getAirport_out().getAirport_id()));
			t.add(Integer.toString(c.getAirport_in().getAirport_id()));
			t.add(new SimpleDateFormat("dd.MM.yyyy HH:mm").format(c.getDate()));
			t.add(Integer.toString(c.getLength()));
			t.add(Integer.toString(c.getPlane().getPlane_id()));
			result.add(t);
			result.add(errors);
		} catch (Exception e) {
			if (errors.size() == 0)
				errors.add("43");
			result.add(errors);
		}
		return result;
	}

	@RequestMapping(value="/remove_flight", method=RequestMethod.POST)
	public @ResponseBody List<Integer> remove_flight(HttpServletRequest request, Integer flight_id) {
		List<Integer> errors = new ArrayList<Integer>();
		try {
			if (!is_admin(request)) {
				errors.add(45);
				throw new Exception();
			}
			Flights c = flights.getById(flight_id);
			if (c == null) {
				errors.add(50);
				throw new Exception();
			}
			flights.delete(c);
		} catch (Exception e) {
			if (errors.size() == 0)
				errors.add(43);
		}
		return errors;
	}

	@RequestMapping(value="/edit_flight", method=RequestMethod.POST)
	public @ResponseBody List<Integer> edit_flight(HttpServletRequest request, FlightsEditForm flightsEditForm) {
		List<Integer> errors = new ArrayList<Integer>();
		try {
			if (!is_admin(request)) {
				errors.add(45);
				throw new Exception();
			}
			String id_s = flightsEditForm.getId();
			String company_s = flightsEditForm.getCompany();
			String a0_s = flightsEditForm.getA0();
			String a1_s = flightsEditForm.getA1();
			String date_s = flightsEditForm.getDate();
			String length_s = flightsEditForm.getLength();
			String plane_s = flightsEditForm.getPlane();
			if (company_s == "" || a0_s == "" || id_s == "" || a1_s == "" || plane_s == "" || date_s == "" || length_s == "") {
				errors.add(53);
				throw new Exception();
			}
			Integer id = null;
			Integer company_id = null;
			Integer a0_id = null;
			Integer a1_id = null;
			Date date = null;
			Integer length = null;
			Integer plane_id = null;
			try {
				id = Integer.parseInt(id_s);
				company_id = Integer.parseInt(company_s);
				a0_id = Integer.parseInt(a0_s);
				a1_id = Integer.parseInt(a1_s);
				length = Integer.parseInt(length_s);
				plane_id = Integer.parseInt(plane_s);
			} catch (Exception e) {
				errors.add(47);
				throw e;
			}
			try {
				date = new SimpleDateFormat("dd.MM.yyyy HH:mm").parse(date_s);
			} catch (Exception e) {
				errors.add(54);
			}
			Companies company = companies.getById(company_id);
			if (company == null) {
				errors.add(16);
			}
			Airports a0 = airports.getById(a0_id);
			if (a0 == null) {
				errors.add(55);
			}
			Airports a1 = airports.getById(a1_id);
			if (a1 == null) {
				errors.add(56);
			}
			Planes plane = planes.getById(plane_id);
			if (plane == null) {
				errors.add(57);
			}
			Flights c = flights.getById(id);
			if (c == null) 
				errors.add(58);

			if (errors.size() != 0)
				throw new Exception();

			c.setCompany(company);
			c.setAirport_out(a0);
			c.setAirport_in(a1);
			c.setDate(date);
			c.setLength(length);
			c.setPlane(plane);
			flights.update(c);
		} catch (Exception e) {
			if (errors.size() == 0)
				errors.add(43);
		}
		return errors;
	}

	@RequestMapping(value="/get_tickets", method=RequestMethod.GET)
	public @ResponseBody List<List<List<String>>> get_tickets(HttpServletRequest request, Integer flight_id) {
		List<String> errors = new ArrayList<String>();
		List<List<String>> ts = new ArrayList<List<String>>();
		List<List<List<String>>> result = new ArrayList<List<List<String>>>();
		try {
			if (!is_admin(request)) {
				errors.add("45");
				throw new Exception();
			}
			List<Tickets> t = tickets.getByFlight(flights.getById(flight_id));
			// System.out.println("result size = " + t.size());
			for (int i = 0; i < t.size(); i++) {
				List<String> ticket = new ArrayList<String>(6);
				ticket.add(String.valueOf(t.get(i).getTicket_id()));
				if (t.get(i).getUser() != null)
					ticket.add(String.valueOf(t.get(i).getUser().getUser_id()));
				else
					ticket.add("null");
				ticket.add(String.valueOf(t.get(i).getSeat()));
				ticket.add(String.valueOf(t.get(i).getType()));
				ticket.add(String.valueOf(t.get(i).getCost()));
				ticket.add(String.valueOf(t.get(i).getPaid()));
				ts.add(ticket);
			}
			result.add(ts);
		} catch (Exception e) {
			System.out.println("error: " + e.getMessage());
			if (errors.size() == 0)
				errors.add("43");
		} finally {
			List<List<String>> re_e = new ArrayList<List<String>>();
			re_e.add(errors);
			result.add(re_e);
		}

		return result;
	}

	@RequestMapping(value="/rm_ticket", method=RequestMethod.POST)
	public @ResponseBody String rm_ticket(HttpServletRequest request, int ticket_id) {
		try {
			if (!is_admin(request))
				throw new Exception();
			Tickets t = tickets.getById(ticket_id);
			if (t == null)
				throw new Exception();
			tickets.delete(t);
		} catch (Exception e) {
			return "error";
		}
		return "ok";
	}

	@RequestMapping(value="/add_ticket", method=RequestMethod.POST)
	public @ResponseBody String add_ticket(HttpServletRequest request, AddTicketForm addTicketForm) {
		try {
			if (!is_admin(request))
				throw new Exception();
			System.out.println(addTicketForm.getFlight_id());
			int flight_id = Integer.parseInt(addTicketForm.getFlight_id());
			int seat = Integer.parseInt(addTicketForm.getSeat());
			int type = Integer.parseInt(addTicketForm.getType());
			double cost = Double.parseDouble(addTicketForm.getCost());
			Tickets t = new Tickets();
			Flights f = flights.getById(flight_id);
			if (f == null)
				throw new Exception();
			t.setFlight(f);
			t.setSeat(seat);
			t.setType(type);
			t.setCost(cost);
			t.setPaid(0);
			t.setUser(null);
			tickets.add(t);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return "error";
		}
		return "ok";
	}
}
