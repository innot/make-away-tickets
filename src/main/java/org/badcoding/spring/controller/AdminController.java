package org.badcoding.spring.controller;

import javax.servlet.http.*;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/admin")
public class AdminController {
	@RequestMapping("/logout")
	public String logout(HttpServletRequest request) {
		HttpSession session = request.getSession();
		session.setAttribute("is_admin", null);
        return "redirect:/index";
	}

	@RequestMapping("/")
	public String root() {
        return "redirect:/admin/companies";
	}
}
