package org.badcoding.spring.controller;

import java.util.*;
import java.util.regex.*;
import java.text.*;
import javax.servlet.http.*;
import java.math.BigInteger;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.annotation.*;
import org.badcoding.dao.interfaces.*;
import org.badcoding.hibernate.stored.*;
import org.badcoding.spring.form.*;
import java.security.SecureRandom;

@Controller
@RequestMapping("/user")
public class UserController {
	@Autowired
	UsersDAO users;

	@Autowired
	TicketsDAO tickets;

	@Autowired
	FlightsDAO flights;

	@Autowired
	CompaniesDAO companies;

	@Autowired
	PointsDAO points;

	@Autowired
	CitiesDAO cities;

	@Autowired
	BlockchainAPI api;

	private SecureRandom random = new SecureRandom();

	private Integer session_id(HttpServletRequest request) {
		HttpSession session = request.getSession();
		return (Integer)session.getAttribute("user_id");
	}

	@RequestMapping("/logout")
	public String logout(HttpServletRequest request) {
		HttpSession session = request.getSession();
		session.setAttribute("user_id", null);
        return "redirect:/index";
	}

	@RequestMapping("/")
	public String root() {
        return "redirect:/user/settings";
	}

	@RequestMapping("/order_ticket")
	public String order_ticket(@RequestParam int ticket_id, HttpServletRequest request, Map<String, Object> model) {
		Integer uid = session_id(request);
		List<Integer> errors = new ArrayList<Integer>();
		if (uid == null || users.getById(uid) == null) {
			errors.add(44);
			model.put("info", errors);
			model.put("ticket_id", ticket_id);
        	return "redirect:/login";
		}
		Tickets t = tickets.getById(ticket_id);
		t.setUser(users.getById(uid));
		String secret = new BigInteger(130, random).toString(32).substring(0, 20);
		t.setPayment_secret(secret);
		try {
			t.setDestination_wallet(api.generatePaymentAddress(ticket_id, secret, false));
			tickets.update(t);
			model.put("ticket_id", ticket_id);
		} catch (Exception e) {
			System.out.println("exception caught: " + e.getMessage());
		}
		return "redirect:/user/tickets";
	}

	@RequestMapping("/settings")
	public String settings(HttpServletRequest request, Map<String, Object> model) {
		Integer uid = session_id(request);
		List<Integer> errors = new ArrayList<Integer>();
		if (uid == null) {
			errors.add(19);
			model.put("e", errors);
			return "redirect:/index";
		}
		if (users.getById(uid) == null) {
			errors.add(31);
			model.put("e", errors);
			return "redirect:/index";
		}
		model.put("settingsForm", new SettingsForm());
		return "/user/settings";
	}

	@RequestMapping(value="/add_points", method=RequestMethod.POST)
	public @ResponseBody List<Integer> add_points(HttpServletRequest request, @RequestParam String code) {
		List<Integer> errors = new ArrayList<Integer>();
		try {
			Integer uid = session_id(request);
			if (uid == null) {
				errors.add(40);
				throw new Exception();
			}
			Users user = users.getById(uid);
			if (user == null) {
				errors.add(31);
				throw new Exception();
			}
			points.add(uid, code);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			if (errors.size() == 0)
				errors.add(43);
		}
		return errors;
	}

	@RequestMapping(value="/pay_with_points", method=RequestMethod.POST)
	public @ResponseBody String add_points(HttpServletRequest request, @RequestParam String ticket_id, @RequestParam String value) {
		try {
			Integer uid = session_id(request);
			if (uid == null)
				throw new Exception();
			Users user = users.getById(uid);
			if (user == null)
				throw new Exception();
			Tickets t = tickets.getById(Integer.parseInt(ticket_id));
			if (t == null)
				throw new Exception();
			Points p = points.getByUserAndCompany(uid, t.getFlight().getCompany().getCompany_id());
			Double v = Double.parseDouble(value);
			if (p == null || p.getValue() < v)
				return "error";
			p.setValue((int)(p.getValue() - v));
			points.update(p);
			double total_pts = t.getFlight().getLength() / 1000.0 / t.getFlight().getCompany().getBonus_points_ratio();
			double paid = t.getCost() * v / total_pts;
			t.setPaid(t.getPaid() + paid);
			tickets.update(t);
		} catch (Exception e) {
			return "error";
		}
		return "ok";
	}

	@RequestMapping(value="/ticket_info", method=RequestMethod.GET)
	public @ResponseBody List<List<String>> ticket_info(HttpServletRequest request, @RequestParam int ticket_id) throws Exception {
		Integer uid = session_id(request);
		if (uid == null)
			throw new Exception();
		Users user = users.getById(uid);
		if (user == null)
			throw new Exception();
		Tickets t = tickets.getById(ticket_id);
		if (uid != t.getUser().getUser_id())
			throw new Exception();

		List<List<String>> result = new ArrayList<List<String>>();

		List<String> tmp = new ArrayList<String>(2);
		tmp.add("ticket-id-div"); 
		tmp.add(((Integer)(t.getTicket_id())).toString());
		result.add(tmp);

		tmp = new ArrayList<String>(2);
		tmp.add("city0"); 
		tmp.add(t.getFlight().getAirport_out().getCity().getName());
		result.add(tmp);

		tmp = new ArrayList<String>(2);
		tmp.add("flight_id"); 
		tmp.add(((Integer)(t.getFlight().getFlight_id())).toString());
		result.add(tmp);

		tmp = new ArrayList<String>(2);
		tmp.add("seat"); 
		tmp.add(((Integer)(t.getSeat())).toString());
		result.add(tmp);

		tmp = new ArrayList<String>(2);
		tmp.add("city1"); 
		tmp.add(t.getFlight().getAirport_in().getCity().getName());
		result.add(tmp);

		tmp = new ArrayList<String>(2);
		tmp.add("type"); 
		tmp.add(((Integer)(t.getType())).toString());
		result.add(tmp);

		tmp = new ArrayList<String>(2);
		tmp.add("airport0"); 
		tmp.add(t.getFlight().getAirport_out().getTitle());
		result.add(tmp);

		tmp = new ArrayList<String>(2);
		tmp.add("airport1"); 
		tmp.add(t.getFlight().getAirport_in().getTitle());
		result.add(tmp);

		tmp = new ArrayList<String>(2);
		tmp.add("plane"); 
		tmp.add(t.getFlight().getPlane().getTitle());
		result.add(tmp);

		tmp = new ArrayList<String>(2);
		tmp.add("cost"); 
		tmp.add(((Double)(t.getCost())).toString());
		result.add(tmp);

		tmp = new ArrayList<String>(2);
		tmp.add("date"); 
		tmp.add(new SimpleDateFormat("dd.MM.yyyy HH:mm").format(t.getFlight().getDate()));
		result.add(tmp);

		tmp = new ArrayList<String>(2);
		tmp.add("company"); 
		tmp.add(t.getFlight().getCompany().getTitle());
		result.add(tmp);

		tmp = new ArrayList<String>(2);
		tmp.add("length"); 
		tmp.add(((Integer)(t.getFlight().getLength())).toString());
		result.add(tmp);

		tmp = new ArrayList<String>(2);
		tmp.add("paid"); 
		tmp.add(((Double)(t.getPaid())).toString());
		result.add(tmp);

		tmp = new ArrayList<String>(2);
		tmp.add("address"); 
		tmp.add(t.getDestination_wallet());
		result.add(tmp);

		Points p = points.getByUserAndCompany(uid, t.getFlight().getCompany().getCompany_id());
		tmp = new ArrayList<String>(2);
		tmp.add("points_total"); 
		tmp.add(p == null ? "0" : Integer.toString(p.getValue()));
		result.add(tmp);

		tmp = new ArrayList<String>(2);
		tmp.add("points_req"); 
		tmp.add(Double.toString((t.getCost() - t.getPaid()) / t.getCost() * t.getFlight().getLength() / 1000.0 / t.getFlight().getCompany().getBonus_points_ratio()));
		result.add(tmp);

		return result;
	}

	@RequestMapping(value="/edit_user", method=RequestMethod.POST)
	public @ResponseBody List<Integer> edit_user(HttpServletRequest request, SettingsForm settingsForm) {
		List<Integer> errors = new ArrayList<Integer>();
		try {
			Integer uid = session_id(request);
			if (uid == null) {
				errors.add(40);
				throw new Exception();
			}
			String email = settingsForm.getEmail();
			String name = settingsForm.getName();
			String last_name = settingsForm.getLast_name();
			String patronymic = settingsForm.getPatronymic();
			String password = settingsForm.getPassword();
			String re_password = settingsForm.getRe_password();
			String old_password = settingsForm.getOld_password();
			Pattern email_regex = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);

			if (password != "" && !password.equals(re_password))
				errors.add(20);
			if (password != "" && password.length() < 8)
				errors.add(21);
			if (name != "" && name.length() < 4)
				errors.add(22);
			if (last_name != "" && last_name.length() < 4)
				errors.add(23);
			if (email != "" && !email_regex.matcher(email).matches())
				errors.add(24);
			if (email != "" && users.getByEmail(email).size() != 0)
				errors.add(25);

			Users user = users.getById(uid);
			if (user == null) {
				errors.add(31);
				throw new Exception();
			}
			if (!user.checkPassword(old_password)) {
				errors.add(32);
				throw new Exception();
			}
			if (errors.size() != 0)
				throw new Exception();

			if (email != "")
				user.setEmail(email);
			if (name != "")
				user.setName(name);
			if (last_name != "")
				user.setLast_name(last_name);
			if (patronymic != "")
				user.setPatronymic(patronymic);
			if (password != "")
				user.setPassword(password);
			users.update(user);
		} catch (Exception e) {
			if (errors.size() == 0)
				errors.add(43);
		}
		return errors;
	}

	@RequestMapping(value = "/points", method = RequestMethod.GET)
	public String points(HttpServletRequest request, Map<String, Object> model) {
		Integer uid = session_id(request);
		List<Integer> errors = new ArrayList<Integer>();
		if (uid == null) {
			errors.add(19);
			model.put("e", errors);
			return "redirect:/index";
		}
		Users user = users.getById(uid);
		if (user == null) {
			errors.add(31);
			model.put("e", errors);
			return "redirect:/index";
		}
		List<Points> result = points.getByUser(uid, true);
		if (result.size() == 0) {
			errors.add(41);
			model.put("info", errors);
		} else {
			model.put("results", result);
		}

		return "/user/points";
	}

	@RequestMapping(value = "/tickets", method = RequestMethod.GET)
	public String tickets(@RequestParam(value="ticket_id", required=false) Integer ticket_id, HttpServletRequest request, Map<String, Object> model) {
		Integer uid = session_id(request);
		List<Integer> errors = new ArrayList<Integer>();
		if (uid == null) {
			errors.add(19);
			model.put("e", errors);
			return "redirect:/index";
		}
		Users user = users.getById(uid);
		if (user == null) {
			errors.add(31);
			model.put("e", errors);
			return "redirect:/index";
		}
		List<Tickets> result = tickets.getByUser(user, null);
		if (result.size() == 0) {
			errors.add(42);
			model.put("info", errors);
		} else {
			model.put("results", result);
		}
		if (ticket_id != null) {
			model.put("ticket_id", ticket_id);
		}

		return "/user/tickets";
	}
}
