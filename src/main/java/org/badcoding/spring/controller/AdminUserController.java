package org.badcoding.spring.controller;

import java.util.*;
import java.util.regex.*;
import java.text.*;
import javax.servlet.http.*;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.annotation.*;
import org.badcoding.dao.interfaces.*;
import org.badcoding.hibernate.stored.*;
import org.badcoding.spring.form.*;

@Controller
@RequestMapping("/admin")
public class AdminUserController {
	@Autowired
	UsersDAO users;

	@Autowired
	TicketsDAO tickets;

	@Autowired
	FlightsDAO flights;

	@Autowired
	CompaniesDAO companies;

	@Autowired
	PointsDAO points;

	@Autowired
	CitiesDAO cities;

	private Boolean is_admin(HttpServletRequest request) {
		HttpSession session = request.getSession();
		return (session.getAttribute("is_admin") != null);
	}

	@RequestMapping("/users")
	public String users(HttpServletRequest request, Map<String, Object> model) {
		List<Integer> errors = new ArrayList<Integer>();
		if (!is_admin(request)) {
			errors.add(45);
			model.put("e", errors);
			return "redirect:/index";
		}
		model.put("usersForm", new UsersForm());
		return "/admin/users";
	}

	@RequestMapping(value = "/users_search", method = RequestMethod.GET)
	public String users_search(@ModelAttribute UsersForm usersForm, HttpServletRequest request, Map<String, Object> model) {
		List<Integer> errors = new ArrayList<Integer>();
		List<Users> result = new ArrayList<Users>();
		if (!is_admin(request)) {
			errors.add(45);
			model.put("e", errors);
			return "redirect:/index";
		}
		try {
			String id_s = usersForm.getId();
			String first_name = usersForm.getFirst_name();
			String last_name = usersForm.getLast_name();
			String patronymic = usersForm.getPatronymic();
			String flight_s = usersForm.getFlight();
			String company_s = usersForm.getCompany();
			String paid_s = usersForm.getPaid();
			String email = usersForm.getEmail();
			if (!id_s.equals("") && (!first_name.equals("") || !last_name.equals("") || !patronymic.equals("") || !flight_s.equals("") || !company_s.equals("") || !paid_s.equals("") || !email.equals(""))) {
				errors.add(46);
				throw new Exception();
			}
			if (id_s == "")
				id_s = "-1";
			if (flight_s == "")
				flight_s = "-1";
			if (company_s == "")
				company_s = "-1";
			if (paid_s == "")
				paid_s = "-1";

			Integer id = null;
			Integer company = null;
			Integer paid = null;
			Integer flight = null;
			try {
				id = Integer.parseInt(id_s);
				company = Integer.parseInt(company_s);
				paid = Integer.parseInt(paid_s);
				flight = Integer.parseInt(flight_s);
			} catch (Exception e) {
				errors.add(47);
				throw e;
			}
			if (id != -1) {
				Users t = users.getById(id);
				if (t != null)	
					result.add(t);
			} else {
				result = users.getByFilters(first_name, last_name, patronymic, flight, company, paid, email);
			}
			if (result.size() == 0) {
				errors.add(48);
				model.put("info", errors);
			} else {
				model.put("results", result);
			}
		} catch (Exception e) {
			model.put("errors", errors);
		}
		model.put("usersForm", usersForm);
		return "/admin/users";
	}

	@RequestMapping(value="/add_user", method=RequestMethod.POST)
	public @ResponseBody List<Integer> add_user(HttpServletRequest request, UsersEditForm usersEditForm) {
		List<Integer> errors = new ArrayList<Integer>();
		try {
			if (!is_admin(request)) {
				errors.add(45);
				throw new Exception();
			}
			String email = usersEditForm.getEmail();
			String name = usersEditForm.getName();
			String last_name = usersEditForm.getLast_name();
			String patronymic = usersEditForm.getPatronymic();
			String password = usersEditForm.getPassword();

			Pattern email_regex = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);

			if (password.length() < 8)
				errors.add(21);
			if (name.length() < 4)
				errors.add(22);
			if (last_name.length() < 4)
				errors.add(23);
			if (!email_regex.matcher(email).matches())
				errors.add(24);
			if (users.getByEmail(email).size() != 0)
				errors.add(25);

			Users user = new Users();
			user.setEmail(email);
			user.setName(name);
			user.setLast_name(last_name);
			user.setPatronymic(patronymic);
			user.setPassword(password);
			user.setDate_registered(new Date());
			user.setLast_login(null);
			users.add(user);
		} catch (Exception e) {
			if (errors.size() == 0)
				errors.add(43);
		}
		return errors;
	}

	@RequestMapping(value="/get_user", method=RequestMethod.GET)
	public @ResponseBody List<List<String>> get_user(HttpServletRequest request, @RequestParam int user_id) {
		List<String> errors = new ArrayList<String>();
		List<List<String>> result = new ArrayList<List<String>>();
		try {
			if (!is_admin(request)) {
				errors.add("45");
				throw new Exception();
			}
			Users c = users.getById(user_id);
			if (c == null) {
				errors.add("48");
				throw new Exception();
			}
			List<String> t = new ArrayList<String>();
			t.add(Integer.toString(c.getUser_id()));
			t.add(c.getName());
			t.add(c.getLast_name());
			t.add(c.getPatronymic());
			t.add(c.getEmail());
			t.add(c.getLast_login() == null ? "never" : new SimpleDateFormat("dd.MM.yyyy HH:mm:ss").format(c.getLast_login()));
			t.add(new SimpleDateFormat("dd.MM.yyyy HH:mm:ss").format(c.getDate_registered()));
			result.add(t);
			result.add(errors);
		} catch (Exception e) {
			if (errors.size() == 0)
				errors.add("43");
			result.add(errors);
		}
		return result;
	}

	@RequestMapping(value="/remove_user", method=RequestMethod.POST)
	public @ResponseBody List<Integer> remove_user(HttpServletRequest request, Integer user_id) {
		List<Integer> errors = new ArrayList<Integer>();
		try {
			if (!is_admin(request)) {
				errors.add(45);
				throw new Exception();
			}
			Users c = users.getById(user_id);
			if (c == null) {
				errors.add(50);
				throw new Exception();
			}
			users.delete(c);
		} catch (Exception e) {
			if (errors.size() == 0)
				errors.add(43);
		}
		return errors;
	}

	@RequestMapping(value="/edit_user", method=RequestMethod.POST)
	public @ResponseBody List<Integer> edit_user(HttpServletRequest request, UsersEditForm usersEditForm) {
		List<Integer> errors = new ArrayList<Integer>();
		try {
			if (!is_admin(request)) {
				errors.add(45);
				throw new Exception();
			}
			String id_s = usersEditForm.getId();
			String email = usersEditForm.getEmail();
			String name = usersEditForm.getName();
			String last_name = usersEditForm.getLast_name();
			String patronymic = usersEditForm.getPatronymic();

			Pattern email_regex = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);

			if (name.length() < 4)
				errors.add(22);
			if (last_name.length() < 4)
				errors.add(23);
			if (!email_regex.matcher(email).matches())
				errors.add(24);

			Integer id = null;
			try {
				id = Integer.parseInt(id_s);
			} catch (Exception e) {
				errors.add(47);
			}
			if (errors.size() != 0)
				throw new Exception();

			if (email != users.getById(id).getEmail() && users.getByEmail(email).size() != 0)
				errors.add(25);

			Users user = users.getById(id);
			if (user == null) {
				errors.add(52);
				throw new Exception();
			}

			user.setEmail(email);
			user.setName(name);
			user.setLast_name(last_name);
			user.setPatronymic(patronymic);
			users.update(user);
		} catch (Exception e) {
			if (errors.size() == 0)
				errors.add(43);
		}
		return errors;
	}
}

