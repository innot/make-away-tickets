package org.badcoding.spring.form;

public class IndexForm {
	private String city_out;
	private String city_in;
	private String company;
	private String date0;
	private String date1;

	public String getCity_out() {
		return city_out;
	}
	public void setCity_out(String city_out) {
		this.city_out = city_out;
	}
	public String getCity_in() {
		return city_in;
	}
	public void setCity_in(String city_in) {
		this.city_in = city_in;
	}
	public String getCompany() {
		return company;
	}
	public void setCompany(String company) {
		this.company = company;
	}
	public String getDate0() {
		return date0;
	}
	public void setDate0(String date0) {
		this.date0 = date0;
	}
	public String getDate1() {
		return date1;
	}
	public void setDate1(String date1) {
		this.date1 = date1;
	}
}
