package org.badcoding.spring.form;

public class CompaniesForm {
	private String id;
	private String title;
	private String activated_bonus_gt;
	private String sold_tickets_gt;
	private String flights;

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getActivated_bonus_gt() {
		return activated_bonus_gt;
	}
	public void setActivated_bonus_gt(String activated_bonus_gt) {
		this.activated_bonus_gt = activated_bonus_gt;
	}
	public String getSold_tickets_gt() {
		return sold_tickets_gt;
	}
	public void setSold_tickets_gt(String sold_tickets_gt) {
		this.sold_tickets_gt = sold_tickets_gt;
	}
	public String getFlights() {
		return flights;
	}
	public void setFlights(String flights) {
		this.flights = flights;
	}
}

