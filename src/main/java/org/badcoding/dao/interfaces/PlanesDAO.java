package org.badcoding.dao.interfaces;

import org.badcoding.hibernate.stored.Planes;

public interface PlanesDAO {
	public void add(Planes plane);
	public void update(Planes plane);
	public void delete(Planes plane);
	public Planes getById(int id);
}
