package org.badcoding.dao.interfaces;

import java.util.List;

import org.badcoding.hibernate.stored.Users;

public interface UsersDAO {
	public void add(Users user);
	public void update(Users user);
	public void delete(Users user);
	public Users getById(int id);
	
	public List<Users> getByEmail(String email); 
	public List<Users> getByFilters(String first_name, 
			String last_name, String patronymic, int flight, 
			int company, int paid, String email);
}
