package org.badcoding.dao.interfaces;

import java.util.List;

import org.badcoding.hibernate.stored.Cities;

public interface CitiesDAO {
	public void add(Cities city);
	public void update(Cities city);
	public void delete(Cities city);
	public Cities getById(int id);
	
	public List<Cities> getByTitle(String title);
}
