package org.badcoding.dao.interfaces;

import org.badcoding.hibernate.stored.Airports;

public interface AirportsDAO {
	public void add(Airports airport);
	public void update(Airports airport);
	public void delete(Airports airport);
	public Airports getById(int id);
}
