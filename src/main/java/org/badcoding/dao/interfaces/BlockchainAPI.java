package org.badcoding.dao.interfaces;

public interface BlockchainAPI {
    public String generatePaymentAddress(int ticket_id, String secret, boolean anonymous) throws Exception;
	public String getAddress();
}
