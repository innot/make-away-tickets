package org.badcoding.dao.implementation;

import org.badcoding.dao.interfaces.CompaniesDAO;
import org.badcoding.hibernate.stored.Companies;
import org.badcoding.hibernate.logic.Database;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;
import org.hibernate.Query;
import org.hibernate.Session;

@Service
public class CompaniesDAOImplementation implements CompaniesDAO {
public void add(Companies company) {
	Session session = null;
	try {
		session = Database.getFactory().openSession();
		session.beginTransaction();
		session.save(company);
		session.getTransaction().commit();
	} finally {
		if (session != null && session.isOpen())
			session.close();
	}
}

public void update(Companies company) {
	Session session = null;
	try {
		session = Database.getFactory().openSession();
		session.beginTransaction();
		session.update(company);
		session.getTransaction().commit();
	} finally {
		if (session != null && session.isOpen())
			session.close();
	}
}

public void delete(Companies company) {
	Session session = null;
	try {
		session = Database.getFactory().openSession();
		session.beginTransaction();
		session.delete(company);
		session.getTransaction().commit();
	} finally {
		if (session != null && session.isOpen())
			session.close();
	}
}

public Companies getById(int id) {
	Session session = null;
	Companies company = null;
	try {
		session = Database.getFactory().openSession();
		company = (Companies)session.get(Companies.class, id);
	} finally {
		if (session != null && session.isOpen())
  			session.close();
	}
	return company;
}

@SuppressWarnings("unchecked")
public List<Companies> getByFilters(String title, int activated_bonus_gt, int sold_tickets_gt, int flights) {
	Session session = null;
	List<Companies> companies = new ArrayList<Companies>();
	try {
		String hsql_query = 
			" select c "
			+ "from Companies c "
			+ "where ( select count(c.company_id) "
			+ "from Companies c1 " 
			+ ", Flights f "
			+ "where c1.company_id = f.company "
			+ "and c1.company_id = c.company_id ";
		if (flights != -1)
			hsql_query += ") >= :flights ";
		else
			hsql_query += ") > -1 ";

		if (activated_bonus_gt == -1)
			hsql_query += "and c.bonus_points_activated > -1 ";
		else
			hsql_query += "and c.bonus_points_activated >= :activated_bonus_gt ";

		if (sold_tickets_gt == -1)
			hsql_query += "and c.sold_tickets_value > -1 ";
		else
			hsql_query += "and c.sold_tickets_value >= :sold_tickets_gt ";

		if (title != null)
			hsql_query += "and c.title like '%' || :title || '%' ";

		session = Database.getFactory().openSession();
    	session.beginTransaction();
		Query query = session.createQuery(hsql_query);
		if (flights != -1)
			query.setInteger("flights", flights);
		if (title != null)
			query.setString("title", title);
		if (activated_bonus_gt != -1)
			query.setInteger("activated_bonus_gt", activated_bonus_gt);
		if (sold_tickets_gt != -1)
			query.setInteger("sold_tickets_gt", sold_tickets_gt);
		companies = query.list();
		session.getTransaction().commit();
	} finally {
    	if (session != null && session.isOpen()) {
    		session.close();
    	}
	}
	return companies;
}

}
