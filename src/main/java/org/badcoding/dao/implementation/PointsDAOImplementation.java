package org.badcoding.dao.implementation;

import org.badcoding.dao.interfaces.PointsDAO;
import org.badcoding.hibernate.stored.*;
import org.badcoding.hibernate.logic.Database;
import org.badcoding.hibernate.logic.Factory;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;
import org.hibernate.Query;
import org.hibernate.Session;

@Service
public class PointsDAOImplementation implements PointsDAO {

public void update(Points point) {
	Session session = null;
	try {
		session = Database.getFactory().openSession();
		session.beginTransaction();
		session.update(point);
		session.getTransaction().commit();
	} finally {
		if (session != null && session.isOpen())
			session.close();
	}
}

public void delete(Points point) {
	Session session = null;
	try {
		session = Database.getFactory().openSession();
		session.beginTransaction();
		session.delete(point);
		session.getTransaction().commit();
	} finally {
		if (session != null && session.isOpen())
			session.close();
	}
}

@SuppressWarnings("unchecked")
public List<Points> getByUser(int id, boolean show_zeroes) {
	Session session = null;
	List<Points> result = new ArrayList<Points>();
	try {
		String hsql_query = 
				"select p " +
				"from Users u, Points p " +
				"where u.user_id = p.pk.user.user_id " +
				"and u.user_id = :id ";

		if (!show_zeroes)
			hsql_query += "and p.value != 0";

		session = Database.getFactory().openSession();
    	session.beginTransaction();
    	Query query = session.createQuery(hsql_query);
		query.setInteger("id", id);
    	
		result = query.list();
		session.getTransaction().commit();
	} finally {
    	if (session != null && session.isOpen()) {
    		session.close();
    	}
	}
	return result;
}

@SuppressWarnings("unchecked")
public List<Points> getByCompany(int id) {
	Session session = null;
	List<Points> result = new ArrayList<Points>();
	try {
		String hsql_query = 
				"select p " +
				"from Companies c, Points p " +
				"where c.company_id = p.pk.company.company_id " +
				"and c.company_id = :id and p.value != 0";

		session = Database.getFactory().openSession();
    	session.beginTransaction();
    	Query query = session.createQuery(hsql_query);
		query.setInteger("id", id);
    	
		result = query.list();
		session.getTransaction().commit();
	} finally {
    	if (session != null && session.isOpen()) {
    		session.close();
    	}
	}
	return result;
}

@SuppressWarnings("unchecked")
public Points getByUserAndCompany(int user_id, int company_id) {
	Session session = null;
	List<Points> result = new ArrayList<Points>();
	try {
		String hsql_query = 
				"select p " +
				"from Points p " +
				"where p.pk.user.user_id = :user_id " +
				"and p.pk.company.company_id = :company_id";

		session = Database.getFactory().openSession();
    	session.beginTransaction();
    	Query query = session.createQuery(hsql_query);
		query.setInteger("user_id", user_id);
		query.setInteger("company_id", company_id);
    	
		result = query.list();
		session.getTransaction().commit();
	} finally {
    	if (session != null && session.isOpen()) {
    		session.close();
    	}
	}
	return (result.size() == 0 ? null : result.get(0));
}

public void add(int user_id, String code) {
	Session session = null;
	try {
		int value = getValueByCode(code);
		int c_id = getCompanyByCode(code);
		boolean update = true;
		Points points = getByUserAndCompany(user_id, c_id);
		if (points == null) {
			update = false;
			points = new Points();
		}
		points.setUser(Factory.GetInstance().getUsersDAO().getById(user_id));
		Companies company = Factory.GetInstance().getCompaniesDAO().getById(c_id);
		company.setBonus_points_activated(value);
		points.setCompany(company);
		points.setValue(points.getValue() + value);
		
		session = Database.getFactory().openSession();
		session.beginTransaction();
		if (update)
			session.update(points);
		else
			session.save(points);
		session.update(company);
		session.getTransaction().commit();
	} finally {
		if (session != null && session.isOpen())
			session.close();
	}
}

// let it be value | company 
public int getCompanyByCode(String code) {
	return (Math.abs(code.hashCode() & 0xFFFF) % 10) + 1;
}

public int getValueByCode(String code) {
	return Math.abs((code.hashCode() >> 16) % 10);
}

}
