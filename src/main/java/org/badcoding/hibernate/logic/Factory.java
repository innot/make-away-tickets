package org.badcoding.hibernate.logic;

import org.badcoding.dao.interfaces.*;
import org.badcoding.dao.implementation.*;

public class Factory {
	private static CountriesDAO country = null;
	private static TicketsDAO ticket = null;
	private static FlightsDAO flight = null;
	private static CitiesDAO city = null;
	private static UsersDAO user = null;
	private static AirportsDAO airport = null;
	private static CompaniesDAO company = null;
	private static PlanesDAO plane = null;
	private static PointsDAO points = null;
	private static Factory instance = null;

	public static synchronized Factory GetInstance() {
		if (instance == null) {
			instance = new Factory();
		}
		return instance;
	}
	
	public CountriesDAO getCountriesDAO() {
		if (country == null) {
			country = new CountriesDAOImplementation();
		}
		return country;
	}

	public TicketsDAO getTicketsDAO() {
		if (ticket == null) {
			ticket = new TicketsDAOImplementation();
		}
		return ticket;
	}

	public FlightsDAO getFlightsDAO() {
		if (flight == null) {
			flight = new FlightsDAOImplementation();
		}
		return flight;
	}

	public CitiesDAO getCitiesDAO() {
		if (city == null) {
			city = new CitiesDAOImplementation();
		}
		return city;
	}

	public UsersDAO getUsersDAO() {
		if (user == null) {
			user = new UsersDAOImplementation();
		}
		return user;
	}

	public AirportsDAO getAirportsDAO() {
		if (airport == null) {
			airport = new AirportsDAOImplementation();
		}
		return airport;
	}

	public CompaniesDAO getCompaniesDAO() {
		if (company == null) {
			company = new CompaniesDAOImplementation();
		}
		return company;
	}

	public PlanesDAO getPlanesDAO() {
		if (plane == null) {
			plane = new PlanesDAOImplementation();
		}
		return plane;
	}
	
	public PointsDAO getPointsDAO() {
		if (points == null) {
			points = new PointsDAOImplementation();
		}
		return points;
	}

}

