package org.badcoding.hibernate.stored;
import javax.persistence.*;

@Entity
@Table(name = "airports")
public class Airports {
	@Id
	@Column(name = "airport_id")
	private int airport_id;
	@Column(name = "title")
	private String title;
	@ManyToOne
	@JoinColumn(name = "city_id")
	private Cities city;

	public Airports() {}
	public Airports(int airport_id, String title, Cities city) {
		this.airport_id = airport_id;
		this.title = title;
		this.city = city;
	}
	public int getAirport_id() {
		return airport_id;
	}
	public void setAirport_id(int airport_id) {
		this.airport_id = airport_id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public Cities getCity() {
		return city;
	}
	public void setCity(Cities city) {
		this.city = city;
	}
}

