package org.badcoding.hibernate.stored;
import javax.persistence.*;

@Entity
@Table(name = "planes")
public class Planes {
	@Id
	@Column(name = "plane_id")
	private int plane_id;
	@Column(name = "title")
	private String title;

	public Planes() {}
	public Planes(int plane_id, String title) {
		this.plane_id = plane_id;
		this.title = title;
	}
	public int getPlane_id() {
		return plane_id;
	}
	public void setPlane_id(int plane_id) {
		this.plane_id = plane_id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
}

