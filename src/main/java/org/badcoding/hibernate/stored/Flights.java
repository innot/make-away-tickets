package org.badcoding.hibernate.stored;
import java.util.Date;

import javax.persistence.*;

@Entity
@Table(name = "flights")
public class Flights {
	@Id
	@Column(name = "flight_id")
	private int flight_id;
	@ManyToOne
	@JoinColumn(name = "company_id")
	private Companies company;
	@ManyToOne
	@JoinColumn(name = "airport_out_id")
	private Airports airport_out;
	@ManyToOne
	@JoinColumn(name = "airport_in_id")
	private Airports airport_in;
	@Column(name = "date")
	private Date date;
	@Column(name = "length")
	private int length;
	@ManyToOne
	@JoinColumn(name = "plane_id")
	private Planes plane;

	public Flights() {}
	public int getFlight_id() {
		return flight_id;
	}
	public void setFlight_id(int flight_id) {
		this.flight_id = flight_id;
	}
	public Companies getCompany() {
		return company;
	}
	public void setCompany(Companies company) {
		this.company = company;
	}
	public Airports getAirport_out() {
		return airport_out;
	}
	public void setAirport_out(Airports airport_out) {
		this.airport_out = airport_out;
	}
	public Airports getAirport_in() {
		return airport_in;
	}
	public void setAirport_in(Airports airport_in) {
		this.airport_in = airport_in;
	}
	public java.util.Date getDate() {
		return date;
	}
	public void setDate(java.util.Date date) {
		this.date = date;
	}
	public int getLength() {
		return length;
	}
	public void setLength(int length) {
		this.length = length;
	}
	public Planes getPlane() {
		return plane;
	}
	public void setPlane(Planes plane) {
		this.plane = plane;
	}
}

