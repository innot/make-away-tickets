package org.badcoding.hibernate.stored;
import javax.persistence.*;

@Entity
@Table(name = "cities")
public class Cities {
	@Id
	@Column(name = "city_id")
	private int city_id;
	@Column(name = "name")
	private String name;
	@ManyToOne
	@JoinColumn(name="country_id")
	private Countries country;

	public Cities() {}
	public Cities(int city_id, String name, Countries country) {
		this.city_id = city_id;
		this.name = name;
		this.country = country;
	}
	public int getCity_id() {
		return city_id;
	}
	public void setCity_id(int city_id) {
		this.city_id = city_id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Countries getCountry() {
		return country;
	}
	public void setCountry(Countries country) {
		this.country = country;
	}
}

