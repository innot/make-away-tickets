package org.badcoding.hibernate.stored;
import javax.persistence.*;

@Entity
@Table(name = "points")
@AssociationOverrides({
	@AssociationOverride(name = "pk.user", 
		joinColumns = @JoinColumn(name = "user_id")),
	@AssociationOverride(name = "pk.company", 
		joinColumns = @JoinColumn(name = "company_id")) 
})
public class Points {
	@EmbeddedId
	private UserCompany pk = new UserCompany();
	@Column(name = "value")
	private int value;

	public Points() {}

	public Points(Users user, Companies company, int value) {
		this.setUser(user);
		this.setCompany(company);
		this.value = value;	
	}

	@Override
	public boolean equals(Object other) {
	if (other == null) 
		return false;
	
	if (!(other instanceof Points)) 
		return false;
	
	Points point = (Points)other; 
		return (this.getUser().getUser_id() == point.getUser().getUser_id()) &&
			(this.getCompany().getCompany_id() == point.getCompany().getCompany_id()) &&
			(this.value == point.value);
	}

	@Transient
	public Users getUser() {
		return pk.getUser();
	}
	public void setUser(Users user) {
		pk.setUser(user);
	}
	@Transient
	public Companies getCompany() {
		return pk.getCompany();
	}
	public void setCompany(Companies company) {
		pk.setCompany(company);
	}
	public UserCompany getPK() {
		return pk;
	}
	public void setPK(UserCompany pk) {
		this.pk = pk;
	}
	public int getValue() {
		return value;
	}
	public void setValue(int value) {
		this.value = value;
	}
}

