<%@ page language="java" contentType="text/html; charset=UTF-8" import="java.util.*,org.badcoding.hibernate.stored.*" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

	<title><spring:message code="title.index_search" /></title>

    <!-- Bootstrap core CSS -->
    <link href="js/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/dashboard.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>
	<%@include file="include/navbar.jsp" %>

	<!-- ticket search modal -->
	<div id="find-tickets-modal" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
	  <div class="modal-dialog modal-sm">
	    <div class="modal-content" align="center">
			<div class="modal-header" align="left">
				<button class="close" aria-hidden="true" data-dismiss="modal" type="button">
					×
				</button>
				<h4 id="mySmallModalLabel" class="modal-title">
					<spring:message code="label.index.17" />
				</h4>
			</div>
			<p id="find-tickets-div"></p>
	    </div>
	  </div>
	</div>

	<!-- results table design follows -->
	<c:if test="${!empty results}">
		<br />
		<div class="container-fluid">
			<c:forEach items="${results}" var="result">
				<div class="row">
					<div class="col-md-8 col-md-offset-1">
						<table class="table table-bordered">
   							<tbody>
   								<tr>
									<td><b><spring:message code="label.index.5" /></b></td>
									<td><b><spring:message code="label.index.6" /></b></td>
									<td><b><spring:message code="label.index.7" /></b></td>
   								</tr>
   								<tr>
									<td><b><spring:message code="label.index.9" />:</b> ${ result.airport_out.city.name }</td>
									<td><b><spring:message code="label.index.10" />:</b> ${ result.flight_id }</td>
									<td><b><spring:message code="label.index.9" />:</b> ${ result.airport_in.city.name }</td>
   								</tr>
   								<tr>
									<td><b><spring:message code="label.index.11" />:</b> (${ result.airport_out.title })</td>
									<td><b><spring:message code="label.index.14" />:</b> ${ result.plane.title }</td>
									<td><b><spring:message code="label.index.11" />:</b> (${ result.airport_in.title })</td>
   								</tr>
   								<tr>
									<td><b><spring:message code="label.index.12" />:</b> ${ result.date }</td>
   									<td><b><spring:message code="label.index.2" />:</b> ${ result.company.title }</td>
									<td><b><spring:message code="label.index.13" />:</b> ${ result.length }<spring:message code="label.km" /></td>
   								</tr>
   							</tbody>
						</table>
					</div>
					<div class="col-md-2">
						<p>
							<spring:message code="label.index.15" />:<br />
							<div class="btn-group btn-group-xs" id="type_selector_${ result.flight_id }" data-toggle="buttons">
								<label class="btn btn-default" id="cb1_${ result.flight_id }">
									<input type="radio" name="options">1
								</label>
								<label class="btn btn-default" id="cb2_${ result.flight_id }">
									<input type="radio" name="options">2
								</label>
								<label class="btn btn-default" id="cb3_${ result.flight_id }">
									<input type="radio" name="options">3
								</label>
								<label class="btn btn-default active" id="cb0_${ result.flight_id }">
									<input type="radio" name="options">&#8704;
								</label>
							</div>
							<br />
						<p>
						</p>
							<br />
							<button type="button" class="btn btn-info" id="search_btn_${ result.flight_id }">
								<span class="glyphicon glyphicon-search"></span>
								<spring:message code="label.index.16" />:<br />
							</button>
						</p>

					</div>
				</div>
				</p>
			</c:forEach>
		</div>
	</c:if>
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="js/js/bootstrap.min.js"></script>
	<script>
		$.each($(":button[id^='search_btn_']"), function(i, btn) {
			btn.onclick = function () {
				$('#found-tickets-div').remove()
				$('#find-tickets-modal').modal()
				$('<div/>', {
				    id: 'found-tickets-div',
				}).appendTo('#find-tickets-div');
				var i = $(this).attr("id").match(/[\d]+$/)[0]
				var checked = $('#type_selector_' + i.toString()).find("label.active").attr("id").match(/\d/)[0]
				var request = $.getJSON("get_tickets", { flight_id: i, type: (checked == 0 ? -1 : checked) }, function(get_tickets) {
					if (get_tickets.length == 0) {
						$('<div/>', {
							text: "<spring:message code="label.index.18" />"
						}).appendTo('#found-tickets-div');
					} else {
						$('<div/>', {
						    class: 'row ft-output-row',
							id: 'found-tickets-header-row'
						}).appendTo('#found-tickets-div');
						$('<div/>', {
						    class: 'col-md-3',
							text: "<spring:message code="label.index.20" />"
						}).appendTo('#found-tickets-header-row');
						$('<div/>', {
						    class: 'col-md-2',
							text: "<spring:message code="label.index.21" />"
						}).appendTo('#found-tickets-header-row');
						$('<div/>', {
						    class: 'col-md-3',
							text: "<spring:message code="label.index.22" />"
						}).appendTo('#found-tickets-header-row');
						var i;
						for (i = 0; i < get_tickets.length; ++i) {
							var ftri = 'found-tickets-row' + i.toString()
							var ticket = get_tickets[i]
							$('<div/>', {
							    class: 'row results-output-row ft-output-row',
								id: ftri
							}).appendTo('#found-tickets-div');
							$('<div/>', {
							    class: 'col-md-3',
								text: ticket[0]
							}).appendTo('#' + ftri);
							$('<div/>', {
							    class: 'col-md-2',
								text: ticket[1]
							}).appendTo('#' + ftri);
							$('<div/>', {
							    class: 'col-md-3',
								text: ticket[2]
							}).appendTo('#' + ftri);
							$('<div/>', {
							    class: 'col-md-3',
								id: 'buy-ticket-' + i.toString()
							}).appendTo('#' + ftri);
							$('<a/>', {
							    type: 'button', 
								href: '/user/order_ticket?ticket_id=' + ticket[3],
								class: 'btn btn-primary btn-xs',
								text: '<spring:message code="label.index.23" />'
							}).appendTo('#buy-ticket-' + i.toString());
						};
					}
				});
				request.fail(function() {
					$('<div/>', {
						text: "<spring:message code="label.index.19" />",
					}).appendTo('#found-tickets-div');
				})
			}
		})
	</script>
  </body>
</html>
