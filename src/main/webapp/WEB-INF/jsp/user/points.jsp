<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

	<title><spring:message code="title.points" /></title>

    <!-- Bootstrap core CSS -->
    <link href="/js/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="/css/dashboard.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>
	<%@include file="../include/navbar.jsp" %>

	<div class="container-fluid">
		<div class="row">
			<div class="col-sm-3 col-md-2 sidebar">
				<ul class="nav nav-sidebar">
					<li><a href="<c:url value="/user/tickets" />"><spring:message code="label.user.01" /></a></li>
					<li class="active"><a href="<c:url value="/user/points" />"><spring:message code="label.user.02" /></a></li>
					<li><a href="<c:url value="/user/settings" />"><spring:message code="label.user.03" /></a></li>
				</ul>
				<ul class="nav nav-sidebar">
					<li><a href="<c:url value="/user/logout" />"><spring:message code="label.user.12" /></a></li>
				</ul>
			</div>
			<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
				<h1 class="page-header"><spring:message code="label.user.02" /></h1>
				<c:if test="${ !empty info }">
					<p>
					<div class="col-xs-10 col-xs-offset-1">
						<c:forEach items="${ info }" var="error">
							<div class="alert alert-info fade in">
								<button class="close" aria-hidden="true" data-dismiss="alert" type="button">
									×
								</button>
								<spring:message code="error.${ error }" />
							</div>
						</c:forEach>
					</div>
					</p>
				</c:if>
				<div class="row">
					<div class="col-md-8 col-md-offset-2">
						<c:forEach items="${ results }" var="result">
							<div class="row highlight">
								<div class="col-md-3">
									<img class="img-rounded" style="width: 100px; height: 100px;" src="${ result.company.logo_url }" />
								</div>
								<div class="col-md-7">
									<p>
										<b><spring:message code="label.user.10" />:</b> ${ result.company.title }<br />
										<b><spring:message code="label.user.11" />:</b> ${ result.value }
									</p>
								</div>
							</div>
						</c:forEach>
					</div>
				</div>
			</div>

		</div>
	</div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="/js/js/bootstrap.min.js"></script>
	<script>
		var btn = $("#edit_button")[0]
		btn.disabled = false;
		btn.onclick = function() {
			$('#errors_p').remove()
			var btn = $(this)
			btn.button('loading')
			$.post( '<c:url value='/user/edit_user' />'
				, $('#edit_form').serialize()
				, function(data) {
					$('<p/>', {
						id: 'errors_p'
					}).appendTo('#errors_div');
					if (data.length == 0) {
						$('<div/>', {
						    class: 'alert alert-success fade in',
							id: 'alert_success'
						}).appendTo('#errors_p');
						$('#alert_success').append('<button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>')
						window['msg1000'].appendTo('#alert_success')
					} else {
						var i;
						for (i = 0; i < data.length; ++i) {
							var error = data[i]
							$('<div/>', {
							    class: 'alert alert-danger fade in',
								id: 'alert_error' + i.toString()
							}).appendTo('#errors_p');
							$('#alert_error' + i.toString()).append('<button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>')
							window['msg' + error.toString()].appendTo('#alert_error' + i.toString())
						};
					}
					btn.button('reset')
				});
			}
	</script>
	<script>
		var btn = $("#submit_button")[0]
		btn.disabled = false;
		btn.onclick = function() {
			$('#code_errors_p').remove()
			var btn = $(this)
			btn.button('loading')
			$.post( '<c:url value='/user/add_points' />'
				, { code: $('#code').val() }
				, function(data) {
					$('<p/>', {
						id: 'code_errors_p'
					}).appendTo('#code_errors_div');
					if (data.length == 0) {
						$('<div/>', {
						    class: 'alert alert-success fade in',
							id: 'code_alert_success'
						}).appendTo('#code_errors_p');
						$('#code_alert_success').append('<button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>')
						window['msg1000'].appendTo('#code_alert_success')
					} else {
						var i;
						for (i = 0; i < data.length; ++i) {
							var error = data[i]
							$('<div/>', {
							    class: 'alert alert-danger fade in',
								id: 'code_alert_error' + i.toString()
							}).appendTo('#code_errors_p');
							$('#code_alert_error' + i.toString()).append('<button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>')
							window['msg' + error.toString()].appendTo('#code_alert_error' + i.toString())
						};
					}
					btn.button('reset')
				});
			}
	</script>
	<%@include file="../include/errors.jsp" %>
  </body>
</html>
