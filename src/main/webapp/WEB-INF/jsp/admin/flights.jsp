<%@ page language="java" import="org.badcoding.hibernate.stored.*,java.text.*" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

	<title><spring:message code="title.flights" /></title>

    <!-- Bootstrap core CSS -->
    <link href="/js/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="/css/dashboard.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>
	<%@include file="../include/navbar.jsp" %>

	<!-- tickets edit modal -->
	<div id="add-ticket-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="infoModalLabel" aria-hidden="true">
	  <div class="modal-dialog modal-lg">
	    <div id="add-ticket-div" class="modal-content" align="center">
			<div class="modal-header" align="left">
				<button class="close" aria-hidden="true" data-dismiss="modal" type="button">
					×
				</button>
				<h4 id="add-ticket-header" class="modal-title">
					<spring:message code="label.user.01" />
				</h4>
			</div>
				<div id="add-ticket-body" class="modal-body">
				</div>      
				<div id="add-ticket-footer" class="modal-footer">
					<div class="row">
						<form role="form" id="add-ticket-form">
							<div class="col-md-1">
								<button id="add-ticket-btn" type="button" class="btn btn-default"><spring:message code="label.admin.14" /></button>
							</div>
							<div class="col-md-3">
							<input type="hidden" id="t_flight_id" name="flight_id" type="text" class="form-control" placeholder="<spring:message code="label.index.10" />" />
						</div>
							<div class="col-md-2">
								<p><input id="t_seat" name="seat" type="text" class="form-control" placeholder="<spring:message code="label.index.20" />" /></p>
							</div>
							<div class="col-md-2">
								<p><input id="t_type" name="type" type="text" class="form-control" placeholder="<spring:message code="label.index.21" />" /></p>
							</div>
							<div class="col-md-2">
								<p><input id="t_cost" name="cost" type="text" class="form-control" placeholder="<spring:message code="label.index.22" />" /></p>
							</div>
							<div class="col-md-2">
								<div class="hidden" id="tickets-rows">0</div>
							</div>
						</form>
					</div>
				</div>
	    </div>
	  </div>
	</div>

	<!-- flight detail modal -->
	<div id="flight_edit_modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="infoModalLabel" aria-hidden="true">
	  <div class="modal-dialog modal-sm">
	    <div id="info-modal-div" class="modal-content" align="center">
			<div class="modal-header" align="left">
				<button id="modal_close_btn" class="close" aria-hidden="true" data-dismiss="modal" type="button">
					×
				</button>
				<h4 id="flight_edit_header" class="modal-title">
				</h4>
			</div>
				<div id="flight_edit_body" class="modal-body">
					<form role="form" id="flight_form">
						<spring:message code="label.admin.01" var="msg1" />
						<spring:message code="label.admin.17" var="msg17" />
						<spring:message code="label.index.12" var="msg12i" />
						<spring:message code="label.index.13" var="msg13i" />
						<spring:message code="label.index.14" var="msg14i" />
						<spring:message code="label.admin.32" var="msg32" />
						<spring:message code="label.admin.33" var="msg33" />
						<spring:message code="label.admin.13" var="msg13" />
						<spring:message code="label.admin.14" var="msg14" />
						<spring:message code="label.admin.15" var="msg15" />
						<p><input id="id" name="id" type="text" class="form-control" placeholder="${ msg1 }" /></p>
						<p><input id="company" name="company" type="text" class="form-control" placeholder="${ msg17 }" /></p>
						<p><input id="a0" name="a0" type="text" class="form-control" placeholder="${ msg32 }" /></p>
						<p><input id="a1" name="a1" type="text" class="form-control" placeholder="${ msg33 }" /></p>
						<p><input id="date" name="date" type="text" class="form-control" placeholder="${ msg12i } [dd.MM.yyyy HH:mm]" /></p>
						<p><input id="length" name="length" type="text" class="form-control" placeholder="${ msg13i }" /></p>
						<p><input id="plane" name="plane" type="text" class="form-control" placeholder="${ msg14i }" /></p>
					</form>
				</div>      
				<div class="modal-footer">
					<button id="modal_add_btn" type="button" class="btn btn-primary">${ msg14 }</button>
					<button id="modal_edit_btn" type="button" class="btn btn-primary">${ msg13 }</button>
					<button id="modal_remove_btn" type="button" class="btn btn-danger">${ msg15 }</button>
				</div>
	    </div>
	  </div>
	</div>

	<div class="container-fluid">
		<div class="row">
			<!-- navbar -->
			<div class="col-sm-3 col-md-2 sidebar">
				<ul class="nav nav-sidebar">
					<li><a href="<c:url value="/admin/companies" />"><spring:message code="title.companies" /></a></li>
					<li><a href="<c:url value="/admin/users" />"><spring:message code="title.users" /></a></li>
					<li class="active"><a href="<c:url value="/admin/flights" />"><spring:message code="title.flights" /></a></li>
				</ul>
				<ul class="nav nav-sidebar">
					<li><a href="<c:url value="/admin/logout" />"><spring:message code="label.user.12" /></a></li>
				</ul>
			</div>
			<!-- content -->
			<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
				<h1 class="page-header">
					<spring:message code="title.flights" />
					<button id="flight_add_btn" type="button" class="btn btn-default btn-lg">
						<span class="glyphicon glyphicon-plus"></span>
					</button>
				</h1>
				<c:if test="${ !empty info }">
					<p>
					<div class="col-xs-10 col-xs-offset-1">
						<c:forEach items="${ info }" var="error">
							<div class="alert alert-info fade in">
								<button class="close" aria-hidden="true" data-dismiss="alert" type="button">
									×
								</button>
								<spring:message code="error.${ error }" />
							</div>
						</c:forEach>
					</div>
					</p>
				</c:if>
				<c:if test="${ !empty errors }">
					<p>
						<c:forEach items="${errors}" var="error">
							<div class="alert alert-danger fade in">
								<button class="close" aria-hidden="true" data-dismiss="alert" type="button">
									×
								</button>
								<strong><spring:message code="error.00" /></strong> 
								<spring:message code="error.${error}" />
							</div>
						</c:forEach>
					</p>
				</c:if>
			    <div class="container-fluid">
					<div class="row">
						<div class="col-md-10 col-md-offset-1 main">
							<form:form method="get" id="flights_form" commandName="flightsForm" action="/admin/flights_search"  role="form">
								<spring:message code="label.admin.01" var="msg1" />
								<spring:message code="label.index.0" var="msg0i" />
								<spring:message code="label.index.1" var="msg1i" />
								<spring:message code="label.index.2" var="msg2i" />
								<spring:message code="label.index.3" var="msg3i" />
								<spring:message code="label.index.4" var="msg4i" />
								<spring:message code="label.admin.21" var="msg5" />
								<spring:message code="label.admin.22" var="msg6" />
								<spring:message code="label.admin.23" var="msg7" />
								<spring:message code="label.admin.24" var="msg8" />
								<div class="row">
									<div class="col-md-3">
										<p><form:input id="id" path="id" type="text" class="form-control" placeholder="${ msg1 }" /></p>
									</div>
									<div class="col-md-3">
										<p><form:input id="city_out" path="city_out" type="text" class="form-control" placeholder="${ msg0i }" /></p>
									</div>
									<div class="col-md-3">
										<p><form:input id="city_in" path="city_in" type="text" class="form-control" placeholder="${ msg1i }" /></p>
									</div>
									<div class="col-md-3">
										<p><form:input id="company" path="company" type="text" class="form-control" placeholder="${ msg2i }" /></p>
									</div>
								</div>
								<div class="row">
									<div class="col-md-5">
										<p><form:input id="start" path="start" type="text" class="form-control" placeholder="${ msg3i }" /></p>
									</div>
									<div class="col-md-5">
										<p><form:input id="end" path="end" type="text" class="form-control" placeholder="${ msg4i } [dd.MM.yyyy HH:mm]" /></p>
									</div>
									<div class="col-md-2" align="right">
										<p><button class="btn btn-primary" type="submit"><spring:message code="label.admin.06" /></button></p>
									</div>
								</div>
			    			</form:form>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-8 col-md-offset-2">
						<c:forEach items="${ results }" varStatus="status" var="result">
							<div class="row highlight">
								<div class="col-md-5">
									<p>
									<b><spring:message code="label.index.2" />:</b> <a href="/admin/companies_search?title=&activated_bonus_gt=&sold_tickets_gt=&flights=&id=${ result.company.company_id }">${ result.company.title }</a>
									</p>
									<p>
										<b><spring:message code="label.index.11" /> 0:</b> ${ result.airport_out.title }
									</p>
									<p>
										<b><spring:message code="label.index.11" /> 1:</b> ${ result.airport_in.title }
									</p>
								</div>
								<div class="col-md-5">
									<p>
										<b><spring:message code="label.index.13" />:</b> ${ result.length } <spring:message code="label.km" />
									</p>
									<p>
										<b><spring:message code="label.index.12" />:</b> <%= new SimpleDateFormat("dd.MM.yyyy HH:mm:ss").format(((Flights)(pageContext.findAttribute("result"))).getDate()) %>
									<p>
										<b><spring:message code="label.index.14" />:</b> ${ result.plane.title }
									</p>
									</p>
								</div>
								<div class="col-md-1 no-rights">
									<p>
										<b><spring:message code="label.admin.01" />:</b> ${ result.flight_id }
									</p>
								</div>
								<div class="col-md-1 no-rights" align="right">
									<p>
										<button id="rm-${ result.flight_id }-btn" type="button" class="btn btn-default btn-xs<c:if test="${ status.isLast() }"> rm-last</c:if>">
											<span class="glyphicon glyphicon-remove"></span>
										</button>
									</p>
									<p>
										<button id="edit-${ result.flight_id }-btn" type="button" class="btn btn-default btn-xs<c:if test="${ status.isLast() }"> edit-last</c:if>">
											<span class="glyphicon glyphicon-pencil"></span>
										</button>
									</p>
									<p>
										<button id="tickets-${ result.flight_id }-btn" type="button" class="btn btn-default btn-xs">
											<span class="glyphicon glyphicon-wrench"></span>
										</button>
									</p>
								</div>
							</div>
						</c:forEach>
					</div>
				</div>
			</div>

		</div>
	</div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="/js/js/bootstrap.min.js"></script>
	<script>
		$('#modal_add_btn').hide()
		$('#modal_edit_btn').hide()
		$('#modal_remove_btn').hide()
		$('#flight_edit_modal').on('hidden.bs.modal', function (e) {
			$('#id').show()
			$('#modal_add_btn').hide()
			$('#modal_edit_btn').hide()
			$('#modal_remove_btn').hide()
			$('#flight_form :input').each(function() {
				$(this).prop('disabled', false)
				$(this).show()
				$(this).val("")
			});
			$('#flight_form').show()
			$('#errors_p').remove()
		})
	</script>
	<script>
		var btn = $("#flight_add_btn")[0]
		btn.onclick = function() {
			$('#id').hide()
			$('#modal_add_btn').show()
			$('#flight_edit_header').text("<spring:message code="label.admin.14" /> <spring:message code="label.admin.34" />")
			$('#flight_edit_modal').modal()
		}
	</script>
	<script>
		var btn = $("#modal_add_btn")[0]
		btn.disabled = false;
		btn.onclick = function() {
			$('#errors_p').remove()
			var btn = $(this)
			btn.button('loading')
			$.post( '<c:url value='/admin/add_flight' />'
				, $('#flight_form').serialize()
				, function(data) {
					$('<p/>', {
						id: 'errors_p'
					}).appendTo('#flight_edit_body');
					if (data.length == 0) {
						$('<div/>', {
						    class: 'alert alert-success fade in',
							id: 'alert_success'
						}).appendTo('#errors_p');
						$('#alert_success').append('<button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>')
						window['msg1000'].appendTo('#alert_success')
						$('#flight_form :input').each(function() {
							$(this).val("");
						});				
					} else {
						var i;
						for (i = 0; i < data.length; ++i) {
							var error = data[i]
							$('<div/>', {
							    class: 'alert alert-danger fade in',
								id: 'alert_error' + i.toString()
							}).appendTo('#errors_p');
							$('#alert_error' + i.toString()).append('<button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>')
							window['msg' + error.toString()].appendTo('#alert_error' + i.toString())
						};
					}
					btn.button('reset')
				});
			}
	</script>
	<script>
		$.each($(":button[id^='rm-']"), function(i, btn) {
			btn.onclick = function () {
				$('#password').hide()
				$('#flight_form :input').each(function() {
					$(this).prop('disabled', true)
				})
				$('#password').hide()
				$('#flight_edit_header').text("<spring:message code="label.admin.15" /> <spring:message code="label.admin.34" />")
				var i = $(this).attr("id").match(/[\d]+/)[0]
				fill_form(i, '#modal_remove_btn')
			}
		})
	</script>
	<script>
		var btn = $("#modal_remove_btn")[0]
		btn.disabled = false;
		btn.onclick = function() {
			var btn = $(this)
			btn.button('loading')
			var i = $('#id').val().match(/[\d]+/)[0]
			$.post( '<c:url value='/admin/remove_flight' />'
				, { flight_id: i }
				, function(data) {
					$('<p/>', {
						id: 'errors_p'
					}).appendTo('#flight_edit_body');
					if (data.length == 0) {
						$('<div/>', {
						    class: 'alert alert-success fade in',
							id: 'alert_success'
						}).appendTo('#errors_p');
						$('#alert_success').append('<button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>')
						window['msg1000'].appendTo('#alert_success')
						$('#flight_form').hide()
					} else {
						var i;
						for (i = 0; i < data.length; ++i) {
							var error = data[i]
							$('<div/>', {
							    class: 'alert alert-danger fade in',
								id: 'alert_error' + i.toString()
							}).appendTo('#errors_p');
							$('#alert_error' + i.toString()).append('<button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>')
							window['msg' + error.toString()].appendTo('#alert_error' + i.toString())
						};
					}
					btn.button('reset')
					btn.hide()
				});
			}
	</script>
	<script>
		$.each($(":button[id^='edit-']"), function(i, btn) {
			btn.onclick = function () {
				$('#flight_edit_header').text("<spring:message code="label.admin.16" /> <spring:message code="label.admin.34" />")
				$('#id').prop('disabled', true)
				var i = $(this).attr("id").match(/[\d]+/)[0]
				fill_form(i, '#modal_edit_btn')
			}
		})
	</script>
	<script>
		var btn = $("#modal_edit_btn")[0]
		btn.disabled = false;
		btn.onclick = function() {
			$('#errors_p').remove()
			$('#id').prop('disabled', false)
			var btn = $(this)
			btn.button('loading')
			$.post( '<c:url value='/admin/edit_flight' />'
				, $('#flight_form').serialize()
				, function(data) {
					$('#id').prop('disabled', true)
					$('<p/>', {
						id: 'errors_p'
					}).appendTo('#flight_edit_body');
					if (data.length == 0) {
						$('<div/>', {
						    class: 'alert alert-success fade in',
							id: 'alert_success'
						}).appendTo('#errors_p');
						$('#alert_success').append('<button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>')
						window['msg1000'].appendTo('#alert_success')
					} else {
						var i;
						for (i = 0; i < data.length; ++i) {
							var error = data[i]
							$('<div/>', {
							    class: 'alert alert-danger fade in',
								id: 'alert_error' + i.toString()
							}).appendTo('#errors_p');
							$('#alert_error' + i.toString()).append('<button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>')
							window['msg' + error.toString()].appendTo('#alert_error' + i.toString())
						};
					}
					btn.button('reset')
				});
			}
	</script>
	<script>
		function fill_form(i, button) {
			$('#flight_edit_modal').modal()
			var request = $.getJSON("get_flight", { flight_id: i }, function(data) {
				if (data.length == 1) { // error
					$('#flight_form').hide()
					$('<p/>', {
						id: 'errors_p'
					}).appendTo('#flight_edit_body');
					var i;
					for (i = 0; i < data[0].length; ++i) {
						var error = data[0][i]
						$('<div/>', {
						    class: 'alert alert-danger fade in',
							id: 'alert_error' + i.toString()
						}).appendTo('#errors_p');
						window['msg' + error.toString()].appendTo('#alert_error' + i.toString())
					};
				} else {
					var result = data[0]
					$('#id').val(result[0])
					$('#company').val(result[1])
					$('#a0').val(result[2])
					$('#a1').val(result[3])
					$('#date').val(result[4])
					$('#length').val(result[5])
					$('#plane').val(result[6])
					$(button).show()
				}
			});
			request.fail(function() {
				$('#flight_form').hide()
				$('<p/>', {
					id: 'errors_p'
				}).appendTo('#flight_edit_body');
				$('<div/>', {
				    class: 'alert alert-danger fade in',
					id: 'alert_error'
				}).appendTo('#errors_p');
				window['msg00'].appendTo('#alert_error')
			});
		}
	</script>
	<script>
		$.each($(":button[id^='tickets-']"), function(i, btn) {
			btn.onclick = function () {
				$('#found-tickets-div').remove()
				$('#errors_p').remove()
				$('#add-ticket-form :input').each(function() {
					$(this).val("")
				})

				$('#add-ticket-modal').modal()
				$('<div/>', {
				    id: 'found-tickets-div',
				}).appendTo('#add-ticket-body');
				var i = $(this).attr("id").match(/[\d]+/)[0]
				$('#t_flight_id').val(i)
				var request = $.getJSON("get_tickets", { flight_id: i }, function(data) {
					if (data.length == 1) {
						$('#flight_form').hide()
						$('<p/>', {
							id: 'errors_p'
						}).appendTo('#add-ticket-body');
						var i;
						for (i = 0; i < data[0][0].length; ++i) {
							var error = data[0][0][i]
							$('<div/>', {
								class: 'alert alert-danger fade in',
								id: 'alert_error' + i.toString()
							}).appendTo('#errors_p');
							window['msg' + error.toString()].appendTo('#alert_error' + i.toString())
						};
					} else {
						var get_tickets = data[0]

						$('<div/>', {
							class: 'row',
							id: 'found-tickets-header-row'
						}).appendTo('#found-tickets-div');
						$('<div/>', {
							class: 'col-md-1',
							id: 'found-tickets-header-0'
						}).appendTo('#found-tickets-header-row');
						$('<div/>', {
							class: 'col-md-1 results-head',
							text: "<spring:message code="label.login.0" />"
						}).appendTo('#found-tickets-header-row');
						$('<div/>', {
							class: 'col-md-2 results-head',
							text: "<spring:message code="label.admin.35" />"
						}).appendTo('#found-tickets-header-row');
						$('<div/>', {
							class: 'col-md-2 results-head',
							text: "<spring:message code="label.index.20" />"
						}).appendTo('#found-tickets-header-row');
						$('<div/>', {
							class: 'col-md-2 results-head',
							text: "<spring:message code="label.index.21" />"
						}).appendTo('#found-tickets-header-row');
						$('<div/>', {
							class: 'col-md-2 results-head',
							text: "<spring:message code="label.index.22" />"
						}).appendTo('#found-tickets-header-row');
						$('<div/>', {
							class: 'col-md-2 results-head',
							text: "<spring:message code="label.user.15" />"
						}).appendTo('#found-tickets-header-row');
						if (get_tickets.length == 0) {
							$('<div/>', {
								id: "nothing-found-div",
								text: "<spring:message code="label.index.18" />"
							}).appendTo('#found-tickets-div');
						} else {
							var i;
							for (i = 0; i < get_tickets.length; ++i) {
								var ftri = 'found-tickets-row' + i.toString()
								var ticket = get_tickets[i]
								$('<div/>', {
									class: 'row results-output-row',
									id: ftri
								}).appendTo('#found-tickets-div');
								$('<div/>', {
									class: 'col-md-1',
									id: 'rm-ticket-' + i.toString()
								}).appendTo('#' + ftri);
								$('<button/>', {
									type: 'button', 
									id: 'rm-t-btn-' + i + '-' + ticket[0],
									class: 'btn btn-danger btn-xs',
									text: '×'
								}).appendTo('#rm-ticket-' + i.toString());
								$('<div/>', {
									class: 'col-md-1',
									text: ticket[0]
								}).appendTo('#' + ftri);
								if (ticket[1] != "null") {
									$('<div/>', {
										id: 'user-div-' + i,
										class: 'col-md-2',
									}).appendTo('#' + ftri);
									$('<a/>', {
										href: '/admin/users_search?id=' + ticket[1] + '&first_name=&last_name=&patronymic=&flight=&company=&paid=&email=',
										text: ticket[1]
									}).appendTo('#user-div-' + i);
								} else {
									$('<div/>', {
										id: 'user-div-' + i,
										class: 'col-md-2',
										text: ticket[1]
									}).appendTo('#' + ftri);
								}
								$('<div/>', {
									class: 'col-md-2',
									text: ticket[2]
								}).appendTo('#' + ftri);
								$('<div/>', {
									class: 'col-md-2',
									text: ticket[3]
								}).appendTo('#' + ftri);
								$('<div/>', {
									class: 'col-md-2',
									text: ticket[4]
								}).appendTo('#' + ftri);
								$('<div/>', {
									class: 'col-md-2',
									text: ticket[5]
								}).appendTo('#' + ftri);
								$('#tickets-rows').text(i + 1)
							}
						}
					}
					bind_rm_btns()
				});
				request.fail(function() {
					$('<div/>', {
						text: "<spring:message code="label.index.19" />",
					}).appendTo('#found-tickets-div');
				})
			}
		})
	</script>
	<script>
		function bind_rm_btns() {
			$.each($(":button[id^='rm-t-btn-']"), function(i, btn) {
				btn.onclick = function () {
					$('#found-tickets-header-0').text("")
					var btn = $(this)
					btn.button('loading')
					var i = $(this).attr("id").match(/[\d]+$/)[0]
					var n = $(this).attr("id").match(/[\d]+/)[0]
					var request = $.post("rm_ticket", { ticket_id: i }, function(data) {
						if (data == "ok") {
							$('#found-tickets-row' + n).remove()
							$('#found-tickets-header-0').append('<span id="ticket-status-span" class="glyphicon glyphicon-ok form-control-feedback"></span>')
						} else {
							$('#found-tickets-header-0').append('<span id="ticket-status-span" class="glyphicon glyphicon-remove form-control-feedback"></span>')
						}
						btn.button('reset')
					});
					request.fail(function() {
						$('#found-tickets-header-0').append('<span id="ticket-status-span" class="glyphicon glyphicon-remove form-control-feedback"></span>')
						btn.button('reset')
					})
				}
			})
		}
	</script>
	<script>
		$('#add-ticket-btn')[0].onclick = function () {
			$('#found-tickets-header-0').text("")
			var btn = $(this)
			btn.button('loading')
			var request = $.post("add_ticket", $('#add-ticket-form').serialize(), function(data) {
				if (data == "ok") {
					$('#nothing-found-div').remove()
					$('#found-tickets-header-0').append('<span id="ticket-status-span" class="glyphicon glyphicon-ok form-control-feedback"></span>')
								var ftri = 'found-tickets-row' + $('#tickets-rows').text()
								$('<div/>', {
									class: 'row results-output-row',
									id: ftri
								}).appendTo('#found-tickets-div')
								$('<div/>', {
									class: 'col-md-1',
								}).appendTo('#' + ftri);
								$('<div/>', {
									class: 'col-md-1 results-output',
									text: '?'
								}).appendTo('#' + ftri);
								$('<div/>', {
									class: 'col-md-2 results-output',
									text: 'null'
								}).appendTo('#' + ftri);
								$('<div/>', {
									class: 'col-md-2 results-output',
									text: $('#t_seat').val()
								}).appendTo('#' + ftri);
								$('<div/>', {
									class: 'col-md-2 results-output',
									text: $('#t_type').val()
								}).appendTo('#' + ftri);
								$('<div/>', {
									class: 'col-md-2 results-output',
									text: $('#t_cost').val()
								}).appendTo('#' + ftri);
								$('<div/>', {
									class: 'col-md-2 results-output',
									text: "0.0"
								}).appendTo('#' + ftri);
								$('#tickets-rows').text(parseInt($('#tickets-rows').text()) + 1)
								$('#t_seat').val(parseInt($('#t_seat').val()) + 1)
				} else {
					$('#found-tickets-header-0').append('<span id="ticket-status-span" class="glyphicon glyphicon-remove form-control-feedback"></span>')
				}
				btn.button('reset')
			});
			request.fail(function() {
				$('#found-tickets-header-0').append('<span id="ticket-status-span" class="glyphicon glyphicon-remove form-control-feedback"></span>')
				btn.button('reset')
			})
		}
	</script>
	<%@include file="../include/errors.jsp" %>
  </body>
</html>



