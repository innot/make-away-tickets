package org.badcoding.test;
import org.badcoding.hibernate.logic.Factory;
import org.badcoding.hibernate.stored.*;
import org.testng.Assert;
import org.testng.annotations.Test;

public class CountriesDAOTest extends Assert{

	@Test
	public void add() {
		Countries instance = new Countries(6789, "Ax", "http://sdfgfg.jpg");
		Factory.GetInstance().getCountriesDAO().add(instance);
		Countries inserted = Factory.GetInstance().getCountriesDAO().getById(6789);
		assertNotNull(inserted);
		assertEquals(inserted.getCountry_id(), instance.getCountry_id());
	}
	
	@Test(dependsOnMethods = { "update" })
	public void delete() {
		Countries inserted = Factory.GetInstance().getCountriesDAO().getById(6789);
		Factory.GetInstance().getCountriesDAO().delete(inserted);
		Countries removed = Factory.GetInstance().getCountriesDAO().getById(6789);
		assertNull(removed);
	}
	
	
	@Test(dependsOnMethods = { "add" })
	public void update() {
		Countries inserted = Factory.GetInstance().getCountriesDAO().getById(6789);
		inserted.setName("test");
		Factory.GetInstance().getCountriesDAO().update(inserted);
		Countries updated = Factory.GetInstance().getCountriesDAO().getById(6789);
		assertEquals("test", updated.getName());
	}
}

