package org.badcoding.test;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.badcoding.hibernate.logic.Factory;
import org.badcoding.hibernate.stored.*;
import org.testng.Assert;
import org.testng.annotations.*;

import java.text.ParseException;

public class FlightsChoiceTest extends Assert {
	private List<Integer> init_stats;
	private List<Integer> added_airports = new ArrayList<Integer>();
	private int last_added_airport = 1000;
	   	
	private List<Integer> get_initial_statistics() throws ParseException {
		List<Integer> result = new ArrayList<Integer>();
		result.add(test0_provider());
		result.add(test1_provider());
		result.add(test2_provider());
		result.add(test3_provider());
		result.add(test4_provider());
		result.add(test5_provider());
		result.add(test6_provider());
		result.add(test7_provider());
		result.add(test8_provider());
		result.add(test9_provider());
		return result;
	}

	// public Collection<Flights> getByChoice(Date start, Date end, 
	// Airports airport_out, Airports airport_in, Companies company);
	private void add_data(int id, int company_id, int city_id_1, int city_id_2, String date_str) throws Exception {
		Airports ai = Factory.GetInstance().getAirportsDAO().getById(1);
		ai.setCity(Factory.GetInstance().getCitiesDAO().getById(city_id_2));
		ai.setAirport_id(++last_added_airport); 
		Factory.GetInstance().getAirportsDAO().add(ai);
		added_airports.add(last_added_airport);
		Airports ao = Factory.GetInstance().getAirportsDAO().getById(1);
		ao.setCity(Factory.GetInstance().getCitiesDAO().getById(city_id_1));
		ao.setAirport_id(++last_added_airport); 
		Factory.GetInstance().getAirportsDAO().add(ao);
		added_airports.add(last_added_airport);

		Flights instance = Factory.GetInstance().getFlightsDAO().getById(1);
		assertNotNull(instance);
		instance.setCompany(Factory.GetInstance().getCompaniesDAO().getById(company_id));
		instance.setFlight_id(id);
		instance.setAirport_in(ai);
		instance.setAirport_out(ao);
		SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy hh:mm"); 
		Date date = sdf.parse(date_str);
		instance.setDate(date);
		Factory.GetInstance().getFlightsDAO().add(instance);
	}

	@BeforeClass
	public void init() throws Exception {
		init_stats = get_initial_statistics();		
		try {
			add_data(6789, 4, 4, 5, "05.05.1980 05:38");
			add_data(6489, 4, 4, 6, "08.07.1990 15:38");
			add_data(6769, 4, 3, 2, "01.05.2005 05:38");
			add_data(6781, 1, 5, 4, "01.05.2015 09:38");
		} catch (Exception e) {
			clean();
			throw e;
		} 
	}

	private int test_provider(String d1, String d2, int c1_id, int c2_id, int c_id) throws ParseException {
	// 	public Collection<Flights> getByChoice(Date start, Date end, 
	// Airports airport_out, Airports airport_in, Companies company);
		Collection<Flights> result = Factory.GetInstance().getFlightsDAO().getByChoice(
				d1 == null ? null : new SimpleDateFormat("dd.MM.yyyy").parse(d1), 
				d2 == null ? null : new SimpleDateFormat("dd.MM.yyyy").parse(d2), 
				c1_id == -1 ? null : Factory.GetInstance().getCitiesDAO().getById(c1_id),
				c2_id == -1 ? null : Factory.GetInstance().getCitiesDAO().getById(c2_id),
				c_id == -1 ? null : Factory.GetInstance().getCompaniesDAO().getById(c_id)
				);
		if (result == null)
			return 0;
		return result.size();
	}

	@Test
	public void test0() throws ParseException {
		assertEquals(init_stats.get(0) + 4, test0_provider());
	}

	private int test0_provider() throws ParseException {
		return test_provider(null, null, -1, -1, -1);
	}

	@Test
	public void test1() throws ParseException {
		assertEquals(init_stats.get(1) + 4, test1_provider());
	}

	private int test1_provider() throws ParseException {
		return test_provider("01.01.1980", "12.11.2021", -1, -1, -1);
	}

	@Test
	public void test2() throws ParseException {
		assertEquals(init_stats.get(2) + 2, test2_provider());
	}

	private int test2_provider() throws ParseException {
		return test_provider("01.01.2000", "12.11.2021", -1, -1, -1);
	}

	@Test
	public void test3() throws ParseException {
		assertEquals((int)init_stats.get(3), test3_provider());
	}

	private int test3_provider() throws ParseException {
		return test_provider("11.11.2011", "12.11.2011", -1, -1, -1);
	}

	@Test
	public void test4() throws ParseException {
		assertEquals(init_stats.get(4) + 1, test4_provider());
	}

	private int test4_provider() throws ParseException {
		return test_provider("01.01.2000", "12.11.2021", -1, -1, 4);
	}

	@Test
	public void test5() throws ParseException {
		assertEquals((int)init_stats.get(5), test5_provider());
	}

	private int test5_provider() throws ParseException {
		return test_provider("01.01.1900", "12.11.2009", -1, -1, 1);
	}

	@Test
	public void test6() throws ParseException {
		assertEquals(init_stats.get(6) + 2, test6_provider());
	}

	private int test6_provider() throws ParseException {
		return test_provider(null, null, 4, -1, -1);
	}

	@Test
	public void test7() throws ParseException {
		assertEquals((int)init_stats.get(7), test7_provider());
	}

	private int test7_provider() throws ParseException {
		return test_provider(null, null, 4, 4, -1);
	}

	@Test
	public void test8() throws ParseException {
		assertEquals(init_stats.get(8) + 1, test8_provider());
	}

	private int test8_provider() throws ParseException {
		return test_provider(null, null, 4, 5, 4);
	}

	@Test
	public void test9() throws ParseException {
		assertEquals((int)init_stats.get(9), test9_provider());
	}

	private int test9_provider() throws ParseException {
		return test_provider("01.01.1990", null, 4, 5, 4);
	}

	@AfterClass
	public void clean() {
		remove_data(6789);
		remove_data(6489);
		remove_data(6769);
		remove_data(6781);
		for (int i = 0; i < added_airports.size(); i++) {
			Airports tmp = Factory.GetInstance().getAirportsDAO().getById(added_airports.get(i));
			Factory.GetInstance().getAirportsDAO().delete(tmp);
		}
	}

	private void remove_data(int id) {
		Flights inserted = Factory.GetInstance().getFlightsDAO().getById(id);
		if (inserted != null)
			Factory.GetInstance().getFlightsDAO().delete(inserted);
	}
}

